let addPluginInput = document.getElementById("add-plugin-input");
addPluginInput.onchange = async function(e) {
    let file = this.files[0];

    let formData = new FormData();
    formData.append("name", file.name);
    formData.append("file", file);

    fetch("/add-plugin", {
        method: "POST",
        body: formData,
    });

    this.value = null;
};

for (let deletePluginLink of document.querySelectorAll(".plugin-actions .action-delete")) {
    deletePluginLink.onclick = async (e) => {
        e.preventDefault();

        await fetch("/delete-plugin?plugin=" + deletePluginLink.dataset.plugin);
    };
}
