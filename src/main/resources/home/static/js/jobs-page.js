function getClosest(elem, selector) {
	while (elem && elem !== document) {
		if (elem.matches(selector)) {
		    return elem;
		}
		elem = elem.parentNode;
	}
	return null;
}

for (let runLink of document.querySelectorAll(".action-run")) {
    let jobId = runLink.dataset.jobId;
    runLink.onclick = async (e) => {
        await fetch("/api/run-job/" + jobId, {
            method: "POST",
        });
        location.reload();
        e.preventDefault();
    };
}

for (let canvas of document.querySelectorAll(".status-chart")) {
    let successCount = parseInt(canvas.dataset.success);
    let failureCount = parseInt(canvas.dataset.failure);
    let abortedCount = parseInt(canvas.dataset.aborted);

    let totalCount = successCount + failureCount + abortedCount;

    if (totalCount == 0) {
        var datasets = [{
           data: [1],

           backgroundColor: ["#ddd"],

           borderWidth: [0],
       }];
    } else {
        var datasets = [{
            data: [
                successCount,
                abortedCount,
                failureCount,
            ],

            backgroundColor: [
                "#4acd2f",
                "#f39904",
                "#d52c41",
            ],

            borderWidth: [1, 1, 1],
        }];
    }

    let context = canvas.getContext("2d");

    let chart = new Chart(context, {
        type: "doughnut",

        data: {
            datasets,

            labels: [
                "Success",
                "Aborted",
                "Failure",
            ],
        },

        options: {
            responsive: true,
            maintainAspectRatio: false,
            cutoutPercentage: 50,
            legend: {
                display: false,
            },
            tooltips: {
                enabled: false,
            },
            animation: {
                animateRotate: false,
            },
        },
    });
}
