let rootDropZone = document.querySelector(".root-drop-zone");

let dropPlaceholder = document.createElement("div");
dropPlaceholder.classList.add("drop-placeholder");

/*for (let dropZone of document.querySelectorAll(".run-step-drop-zone")) {
    dropZone.dropPlaceholder = document.createElement("div");
    dropZone.dropPlaceholder.classList.add("drop-placeholder");
}*/

let availableRunStepsContainer = document.getElementById("available-run-steps");
let availableRunSteps = availableRunStepsContainer.querySelectorAll(".run-step");

let runStepReadyToDrag = null;
let runStepBeingDragged = null;
let copyRunStep = false;

let initialX = 0;
let initialY = 0;

let grabX = 0;
let grabY = 0;

for (let runStep of document.querySelectorAll(".run-step-drop-zone .run-step")) {
    setDragInitListener(runStep, false);
}

for (let runStep of availableRunSteps) {
    setDragInitListener(runStep, true);
}

function setDragInitListener(runStep, copy) {
    runStep.querySelector("h3").onmousedown = e => {
        copyRunStep = copy;
        runStepReadyToDrag = runStep;
        initialX = e.pageX;
        initialY = e.pageY;
        grabX = e.pageX - runStep.offsetLeft;
        grabY = e.pageY - runStep.offsetTop;
        document.body.classList.add("dragging");
        e.preventDefault();
    };
}

function updatePlaceholder(dropZone, mouseX, mouseY) {
    let dragRect = runStepBeingDragged.getBoundingClientRect();
    let centerX = mouseX;//dragRect.left + dragRect.width / 2;
    let centerY = mouseY;//dragRect.top + dragRect.height / 2;

    let dropRect = dropZone.getBoundingClientRect();

    if (centerX > dropRect.left - 50 && centerX < dropRect.right + 50 && centerY > dropRect.top - 50 && centerY < dropRect.bottom + 50) {
        if (dropPlaceholder.parentNode == null) {
            for (let child of dropZone.children) {
                let childRect = child.getBoundingClientRect();
                if (child.classList.contains("drop-zone-row")) {
                    if (centerY <= childRect.bottom && centerY >= childRect.top) {
                        for (let column of child.children) {
                            let colRect = column.getBoundingClientRect();
                            if (centerX <= colRect.right) {
                                return updatePlaceholder(column, mouseX, mouseY);
                            }
                        }
                        return updatePlaceholder(child.lastElementChild, mouseX, mouseY);
                    }
                }
                if (centerY <= childRect.top + childRect.height / 2) {
                    dropZone.insertBefore(dropPlaceholder, child);
                    return true;
                } else if (centerY <= childRect.top + childRect.height / 2) {
                    return true;
                }
            }
            dropZone.appendChild(dropPlaceholder);
            return true;
        } else {
            let parentRect = dropPlaceholder.parentNode.getBoundingClientRect();

            if (dropPlaceholder.parentNode.parentNode.classList.contains("drop-zone-row")) {

            }

            if (dropPlaceholder.previousElementSibling != null) {
                let prevRect = dropPlaceholder.previousElementSibling.getBoundingClientRect();
                if (dropPlaceholder.previousElementSibling.classList.contains("drop-zone-row")) {
                    if (dragRect.top < prevRect.bottom - 15) {
                        for (let column of dropPlaceholder.previousElementSibling.children) {
                            let colRect = column.getBoundingClientRect();
                            if (dragRect.left + dragRect.width / 2 < colRect.right) {
                                column.appendChild(dropPlaceholder);
                                return true;
                            }
                        }
                        dropPlaceholder.previousElementSibling.lastElementChild.appendChild(dropPlaceholder);
                    }
                } else if (dragRect.top < prevRect.top + prevRect.height * 0.4 - 10) {
                    dropPlaceholder.previousElementSibling.parentNode.insertBefore(dropPlaceholder, dropPlaceholder.previousElementSibling);
                }
            } else if (dropPlaceholder.parentNode != rootDropZone) {
                if (dragRect.top < parentRect.top - 20) {
                    let column = dropPlaceholder.parentNode;
                    let row = column.parentNode;
                    rootDropZone.insertBefore(dropPlaceholder, row);
                    if (column.children.length == 0) {
                        let otherColumn = column.previousElementSibling || column.nextElementSibling;
                        for (let child of otherColumn.children) {
                            rootDropZone.insertBefore(child, row);
                        }
                        dropZone.removeChild(row);
                    }
                }
            }

            if (dropPlaceholder.nextElementSibling != null) {
                let nextRect = dropPlaceholder.nextElementSibling.getBoundingClientRect();
                if (dropPlaceholder.nextElementSibling.classList.contains("drop-zone-row")) {
                    if (dragRect.bottom > nextRect.top + 15) {
                        for (let column of dropPlaceholder.nextElementSibling.children) {
                            let colRect = column.getBoundingClientRect();
                            if (dragRect.left + dragRect.width / 2 < colRect.right) {
                                column.insertBefore(dropPlaceholder, column.firstElementChild);
                                return true;
                            }
                        }
                        dropPlaceholder.nextElementSibling.lastElementChild.insertBefore(dropPlaceholder, dropPlaceholder.nextElementSibling.lastElementChild.firstElementChild);
                    }
                } else if (dragRect.bottom > nextRect.top + nextRect.height * 0.6 + 10) {
                    dropPlaceholder.nextElementSibling.parentNode.insertBefore(dropPlaceholder, dropPlaceholder.nextElementSibling.nextSibling);
                }
            } else if (dropPlaceholder.parentNode != rootDropZone) {
                 if (dragRect.bottom > parentRect.bottom + 20) {
                     let column = dropPlaceholder.parentNode;
                     let row = column.parentNode;
                     rootDropZone.insertBefore(dropPlaceholder, row.nextElementSibling);
                     if (column.children.length == 0) {
                         let otherColumn = column.previousElementSibling || column.nextElementSibling;
                         for (let child of otherColumn.children) {
                             rootDropZone.insertBefore(child, row);
                         }
                         dropZone.removeChild(row);
                     }
                 }
             }
            return true;
        }
    }
    return false;
}

document.addEventListener("mousemove", e => {
    if (runStepReadyToDrag != null) {
        let dx = e.pageX - initialX;
        let dy = e.pageY - initialY;
        let distance = Math.sqrt(dx*dx + dy*dy);
        if (distance > 10) {
            if (copyRunStep) {
                runStepBeingDragged = runStepReadyToDrag.cloneNode(true);

                let body = runStepBeingDragged.querySelector(".run-step-body");
                fetchPluginEditorHtml(body, runStepBeingDragged.dataset.plugin);
            } else {
                runStepBeingDragged = runStepReadyToDrag;
                runStepBeingDragged.parentNode.insertBefore(dropPlaceholder, runStepBeingDragged);
            }
            runStepBeingDragged.classList.remove("run-step-inactive");

            var style = window.getComputedStyle(runStepReadyToDrag, null);
            runStepBeingDragged.style.width = style.getPropertyValue("width");
            runStepBeingDragged.style.transformOrigin = grabX + "px " + grabY + "px";

            runStepBeingDragged.classList.add("dragging");
            document.body.appendChild(runStepBeingDragged);
            runStepReadyToDrag = null;

            dropPlaceholder.style.height = runStepBeingDragged.offsetHeight + "px";
            targetDropZone = null;
        }
    }

    if (runStepBeingDragged != null) {
        runStepBeingDragged.style.left = (e.pageX - grabX) + "px";
        runStepBeingDragged.style.top = (e.pageY - grabY) + "px";
        if (updatePlaceholder(rootDropZone, e.pageX, e.pageY)) {
            runStepBeingDragged.classList.add("can-drop");
        } else {
            runStepBeingDragged.classList.remove("can-drop");
            if (dropPlaceholder.parentNode != null) {
                dropPlaceholder.parentNode.removeChild(dropPlaceholder);
            }
        }
    }
});

document.addEventListener("mouseup", e => {
    if (runStepReadyToDrag != null) {
        document.body.classList.remove("dragging");
        runStepReadyToDrag = null;
    }

    if (runStepBeingDragged != null) {
        document.body.classList.remove("dragging");
        document.body.removeChild(runStepBeingDragged);
        let targetDropZone = dropPlaceholder.parentNode;
        if (targetDropZone != null) {
            runStepBeingDragged.style.width = "auto";
            runStepBeingDragged.style.left = "0";
            runStepBeingDragged.style.top = "0";
            runStepBeingDragged.classList.remove("dragging");

            setDragInitListener(runStepBeingDragged, false);
            targetDropZone.insertBefore(runStepBeingDragged, dropPlaceholder);
            targetDropZone.removeChild(dropPlaceholder);
            targetDropZone = null;
        }
        runStepBeingDragged = null;
    }
});

async function fetchPluginEditorHtml(body, plugin) {
    let editorHtml = await fetch("/api/plugin/" + plugin + "/editorHtml");
    body.innerHTML = await editorHtml.text();

    for (let editor of body.querySelectorAll(".script-editor")) {
        let aceEditor = ace.edit(editor);

        aceEditor.setTheme("ace/theme/xcode");
        aceEditor.session.setMode("ace/mode/sh");
        aceEditor.setOptions({
            enableLiveAutocompletion: true,
        });
    }
}
