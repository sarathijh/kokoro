if (jobRunInfo.numberOfRuns.total == 0) {
    var datasets = [{
       data: [1],

       backgroundColor: ["#999"],

       hoverBackgroundColor: ["#999"],

       borderWidth: [0],

       label: "Success vs. Failure",
   }];
} else {
    var datasets = [{
        data: [
            jobRunInfo.numberOfRuns.passed,
            jobRunInfo.numberOfRuns.failed,
        ],

        backgroundColor: [
            "#00a22e",
            "#c30023",
        ],

        hoverBackgroundColor: [
            "#00a22e",
            "#c30023",
        ],

        borderWidth: [0, 0],

        label: "Success vs. Failure",
    }];
}

var config = {
    type: "doughnut",

    data: {
        datasets,

        labels: [
            "Success",
            "Failure",
        ],
    },

    options: {
        responsive: true,
        cutoutPercentage: 80,
        legend: {
            display: false,
        },
        tooltips: {
            enabled: false,
        },
        animation: {
            animateRotate: false,
        },
    },
};

let context = document.getElementById("successRatioChart").getContext("2d");
let chart = new Chart(context, config);
