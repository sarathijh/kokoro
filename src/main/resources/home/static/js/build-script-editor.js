var editor = ace.edit("editor");

editor.setTheme("ace/theme/xcode");
editor.session.setMode("ace/mode/sh");
//editor.renderer.setScrollMargin(0, 300);
editor.setOptions({
    enableLiveAutocompletion: true,
});

/*var staticWordCompleter = {
    getCompletions: function(editor, session, pos, prefix, callback) {
        callback(null, bashCommands.map(function(word) {
            return {
                caption: word,
                value: word,
                meta: "command",
                score: -word.length,
            };
        }));
    }
}

var langTools = ace.require("ace/ext/language_tools");
langTools.addCompleter(staticWordCompleter);*/
