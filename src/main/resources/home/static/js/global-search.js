let searchOpen = false;
let searchBackground = null;
let searchInput = null;

document.addEventListener("keydown", e => {
    if (!searchOpen && e.key == "o" && e.metaKey && e.shiftKey) {
        searchOpen = true;
        if (searchBackground == null) {
            searchBackground = document.createElement("div");
            searchBackground.classList.add("global-search-background");

            let searchWrapper = document.createElement("div");
            searchWrapper.classList.add("search-wrapper");
            searchBackground.appendChild(searchWrapper);

            let searchBox = document.createElement("div");
            searchBox.classList.add("search-box");
            searchWrapper.appendChild(searchBox);

            let searchIcon = document.createElement("div");
            searchIcon.classList.add("icon", "icon-24", "icon-search");
            searchBox.appendChild(searchIcon);

            searchInput = document.createElement("input");
            searchInput.placeholder = "Search actions...";
            searchBox.appendChild(searchInput);

            document.body.appendChild(searchBackground);
        }
        searchBackground.classList.add("visible");
        searchInput.focus();

    } else if (searchOpen && e.key == "Escape") {
        searchOpen = false;
        searchBackground.classList.remove("visible");
    }
});
