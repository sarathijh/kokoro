package database

import Kokoro
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File

object Jobs : IntIdTable() {
    val name = varchar("name", length = 256)
    val type = enumerationByName("type", 25, JobType::class.java)
}

enum class JobType {
    Build,
    Test,
}

val JobType.iconClass get() = when (this) {
    JobType.Build -> "icon-build"
    JobType.Test -> "icon-test"
}

object Configurations : IntIdTable() {
    val job = reference("job", Jobs)
    val datetime = datetime("datetime")
    val buildScript = blob("buildScript")
}

object Runs : IntIdTable() {
    val job = reference("job", Jobs)
    val configuration = reference("configuration", Configurations)
    val startDatetime = datetime("startDatetime")
    val endDatetime = datetime("endDatetime").nullable()
    val status = enumerationByName("status", 25, RunStatus::class.java)
}

object Repositories : IntIdTable() {
    val remoteUrl = varchar("remoteUrl", 255)
}

enum class RunStatus {
    InProgress,
    Passed,
    Failed,
    Aborted,
}

class Repository(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Repository>(Repositories)

    var remoteUrl by Repositories.remoteUrl
}

class Job(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Job>(Jobs)

    var name by Jobs.name
    var type by Jobs.type
    val configurations by Configuration referrersOn Configurations.job
    val runs by Run referrersOn Runs.job
}

class Configuration(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Configuration>(Configurations)

    var job by Job referencedOn Configurations.job
    var datetime by Configurations.datetime
    var buildScript by Configurations.buildScript
}

// TODO: Added run id local to job
class Run(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Run>(Runs)

    var job by Job referencedOn Runs.job
    var configuration by Configuration referencedOn Runs.configuration
    var startDatetime by Runs.startDatetime
    var endDatetime by Runs.endDatetime
    var status by Runs.status
}

val Job.lastRun get() = transaction { runs.maxBy { it.startDatetime } }

val Job.badgeFile get() =  when (lastRun?.status) {
    RunStatus.Passed -> File(Kokoro.homeDirectory, "static/images/badge-build-passing.svg")
    RunStatus.Failed -> File(Kokoro.homeDirectory, "static/images/badge-build-failing.svg")
    RunStatus.InProgress -> File(Kokoro.homeDirectory, "static/images/badge-build-running.svg")
    else -> File(Kokoro.homeDirectory, "static/images/badge-build-failing.svg")
}
