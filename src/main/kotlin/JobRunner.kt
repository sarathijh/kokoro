import database.Configuration
import database.Job
import database.Run
import database.RunStatus
import kotlinx.coroutines.experimental.launch
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import javax.sql.rowset.serial.SerialBlob

object JobRunner {

    private val jobWorkers = mutableMapOf<Job, kotlinx.coroutines.experimental.Job>()

    fun run(jobToRun: Job) {
        if (jobToRun in jobWorkers) {
            println("Failed to run job #${jobToRun.id}: a job can only be run once at a time!")
            return
        }

        val run = transaction {
            Run.new {
                job = jobToRun
                configuration = Configuration.new {
                    job = jobToRun
                    datetime = DateTime()
                    buildScript = SerialBlob(byteArrayOf())
                }
                startDatetime = DateTime()
                endDatetime = null
                status = RunStatus.InProgress
            }
        }

        println("Running job #${jobToRun.id} run #${run.id}")

        jobWorkers[jobToRun] = launch {
            transaction {
                run.status = RunStatus.Passed
            }
        }
    }
}
