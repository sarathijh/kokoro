object GlobalSearch {
    private val actions = mutableMapOf<String, Action>()

    fun register(term: String, action: Action) {
        actions[term] = action
    }

    fun queryAction(query: String) = actions
            .map { bigramComparison(it.key, query) to it.value }
            .filter { it.first > 0.01 }
            .sortedByDescending { it.first }
            .map { it.second }

    private fun bigramComparison(lhs: String, rhs: String): Float {
        val leftBigrams = lhs.bigrams()
        val rightBigrams = rhs.bigrams()

        return (2f * leftBigrams.intersect(rightBigrams).size) / (leftBigrams.size + rightBigrams.size)
    }

    private fun String.bigrams() = windowed(2)

    sealed class Action {
        data class NavigateToUrl(val url: String) : Action() {
            val kind = "NavigateToUrl"
        }
    }
}
