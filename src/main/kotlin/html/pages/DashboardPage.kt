package html.pages

import kotlinx.html.DIV
import kotlinx.html.HEAD
import kotlinx.html.styleLink

class DashboardPage : HtmlPage() {
    override val pageTitle = "Dashboard"
    override val iconClass = "icon-dashboard"
    override val activeSidebarItem = Sidebar.Dashboard

    override fun HEAD.head() {
        styleLink("/static/css/dashboard-page.css")
    }

    override fun DIV.content() {

    }
}
