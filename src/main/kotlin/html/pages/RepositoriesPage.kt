package html.pages

import database.Repository
import org.jetbrains.exposed.sql.transactions.transaction

class RepositoriesPage : ItemsTablePage<Repository>() {
    override val namePlural = "Repositories"
    override val nameSingular = "Repository"
    override val description = "Repositories let your jobs retrieve code from source control services, such as GitHub."
    override val iconClass = "icon-code"
    override val activeSidebarItem = Sidebar.Repositories

    override val items = transaction { Repository.all().toList() }

    override val columns = listOf(
            Column("Remote URL", "remote-url") { repository ->
                +repository.remoteUrl
            }
    )
}
