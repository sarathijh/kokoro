package html.pages

import kotlinx.html.*

abstract class ItemsTablePage<T> : HtmlPage() {
    abstract val namePlural: String
    abstract val nameSingular: String
    abstract val description: String
    override val pageTitle get() = namePlural

    abstract val columns: List<Column>
    abstract val items: List<T>

    open fun itemCssClasses(item: T) = ""

    open fun HEAD.styleSheets() {}

    override fun HEAD.head() {
        styleLink("/static/css/items-table-page.css")
        styleSheets()
    }

    override fun DIV.header() {
        a(href = "#", classes = "button main-button") {
            div(classes = "icon icon-10 icon-plus")
            div { +"Add $nameSingular" }
        }
    }

    override fun DIV.content() {
        if (items.isEmpty()) {
            div(classes = "no-items") {
                div {
                    +description
                    br
                    br
                    +"Click "
                    strong { +"Add $nameSingular" }
                    +" above to get started."
                }
            }
        } else {
            itemsTable()
        }
    }

    private fun DIV.itemsTable() {
        div {
            style = "text-align: right; margin-bottom: 10px;"
            if (items.size == 1) {
                +"${items.size} $nameSingular"
            } else {
                +"${items.size} $namePlural"
            }
        }
        div(classes = "items-table-wrapper") {
            table(classes = "items-table") {
                thead {
                    tr {
                        headerColumns()
                    }
                }
                tbody {
                    for (item in items) {
                        tr(classes = itemCssClasses(item)) {
                            td(classes = "column-checkbox") { input(type = InputType.checkBox) }
                            for (column in columns) {
                                td(classes = "column-${column.cssClass}") {
                                    style = "vertical-align: top;"
                                    column.content(this, item)
                                }
                            }
                        }
                    }
                }
                tfoot {
                    tr {
                        headerColumns()
                    }
                }
            }
        }
    }

    private fun TR.headerColumns() {
        td(classes = "column-checkbox") { input(type = InputType.checkBox) }
        for (column in columns) {
            td(classes = "column-${column.cssClass}") { +column.name }
        }
    }

    inner class Column(val name: String, val cssClass: String, val content: TD.(item: T) -> Unit)
}
