package html.pages

import kotlinx.html.*

class SettingsPage : HtmlPage() {
    override val pageTitle = "Settings"
    override val iconClass = "icon-gear"
    override val activeSidebarItem = Sidebar.Settings

    override fun HEAD.head() {
        styleLink("/static/css/settings-page.css")
    }

    override fun DIV.content() {
        div(classes = "settings-wrapper") {
            ul(classes = "settings-navigation") {
                li { +"General" }
                li { +"Section 2" }
                li { +"Section 3" }
                li { +"Section 4" }
                li { +"Section 5" }
            }
        }
    }
}
