package html.pages

import Kokoro
import kotlinx.html.*
import plugin.PluginWrapper

class PluginsPage : ItemsTablePage<PluginWrapper>() {
    override val namePlural = "Plugins"
    override val nameSingular = "Plugin"
    override val description = "Plugins allow you to add additional functionality to Kokoro."
    override val iconClass = "icon-plugin"
    override val activeSidebarItem = Sidebar.Plugins

    override fun HEAD.styleSheets() {
        styleLink("/static/css/plugins-page.css")
    }

    override fun BODY.foot() {
        script(src = "/static/js/plugins-page.js") {}
    }

    override fun DIV.header() {
        a(href = "#", classes = "button main-button") {
            input(type = InputType.file) {
                id = "add-plugin-input"
                accept = ".jar"
            }
            div(classes = "icon icon-10 icon-plus")
            div { +"Add $nameSingular" }
        }
    }

    override val items = Kokoro.pluginManager.plugins.sortedBy { it.name }

    override val columns = listOf(
            Column("Plugin", "plugin") { plugin ->
                try {
                    val pluginName = plugin.name

                    strong(classes = "plugin-name") {
                        div { +pluginName }
                        //div(classes = "icon icon-12 icon-lock") { title = "Verified core plugin" }
                    }

                    ul(classes = "item-actions") {
                        li {
                            a(href = "#", classes = "action-deactivate") {
                                attributes["data-plugin"] = plugin.hash

                                +"Deactivate"
                            }
                        }
                        li {
                            a(href = "#") { +"Usage" }
                        }
                        /*li {
                            a(href = "#", classes = "action-delete") {
                                attributes["data-plugin"] = plugin.hash

                                +"Delete"
                            }
                        }*/
                    }
                } catch (error: Throwable) {
                    error.printStackTrace()
                }
            },

            Column("Description", "description") { plugin ->
                try {
                    val pluginAuthor = plugin.author
                    val pluginVersion = plugin.version
                    val pluginDescription = plugin.description

                    div { +pluginDescription }
                    div(classes = "item-actions meta-data") { +"Version $pluginVersion | By $pluginAuthor \uD83D\uDD12" }
                    /*div { +"Run steps created by this plugin:" }
                    ul {
                        for (name in runStepNames) {
                            li { +name }
                        }
                    }*/
                } catch (error: Throwable) {
                    error.printStackTrace()
                }
            }
    )
}
