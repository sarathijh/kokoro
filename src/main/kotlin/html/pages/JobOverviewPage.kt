package html.pages

import database.Job
import database.RunStatus
import database.iconClass
import kotlinx.html.*
import org.jetbrains.exposed.sql.transactions.transaction

class JobOverviewPage(private val job: Job) : HtmlPage() {
    override val pageTitle = job.name
    override val iconClass = job.type.iconClass
    override val activeSidebarItem = Sidebar.Jobs

    override fun HEAD.head() {
        styleLink("/static/css/job-overview-page.css")
    }

    override fun DIV.header() {
        img(src="/job/${job.id}/badge") {
            height = "20"
            style = "margin-left: 12px;"
        }
        /*a(href = "/edit-job/${job.id}/run-steps", classes = "button main-button") {
            div(classes = "icon icon-10 icon-pencil")
            div { +"Edit" }
        }*/
    }

    override fun DIV.headerRight() {

    }

    override fun DIV.content() {
        val numberOfRunsPassed = transaction { job.runs.count { it.status == RunStatus.Passed } }
        val numberOfRunsFailed = transaction { job.runs.count { it.status == RunStatus.Failed } }
        val totalNumberOfRuns = numberOfRunsPassed + numberOfRunsFailed

        div(classes = "job-chart") {
            canvas {
                id = "successRatioChart"
                width = "100%"
                height = "100%"
            }
            div(classes = "job-stats") {
                if (totalNumberOfRuns == 0) {
                    div(classes = "no-runs") {
                        +"No runs"
                    }
                } else {
                    div(classes = "passed") { +"$numberOfRunsPassed passed" }
                    div(classes = "failed") { +"$numberOfRunsFailed failed" }
                }
            }
        }

        script {
            unsafe {
                raw("""
                        var jobRunInfo = {
                            numberOfRuns: {
                                total: $totalNumberOfRuns,
                                passed: $numberOfRunsPassed,
                                failed: $numberOfRunsFailed,
                            },
                        };
                    """)
            }
        }
        script(src = "/static/js/Chart.bundle.min.js") {}
        script(src = "/static/js/job-page.js") {}
    }
}
