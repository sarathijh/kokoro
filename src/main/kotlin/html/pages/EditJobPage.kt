package html.pages

import Kokoro
import database.Job
import database.iconClass
import kotlinx.html.*

class EditJobPage(private val job: Job, private val section: String) : HtmlPage() {
    override val pageTitle = "Edit ${job.name}"
    override val iconClass = job.type.iconClass
    override val activeSidebarItem = Sidebar.Jobs

    override fun HEAD.head() {
        styleLink("/static/css/job-configure-page.css")
    }

    override fun DIV.header() {
        div(classes = "icon icon-16 icon-pencil edit-name-button")
    }

    override fun DIV.content() {
        div(classes = "configure-tools") {
            ul(classes = "tabs") {
                li(classes = if (section == "run-steps") "active" else "") { a(href = "/edit-job/${job.id}/run-steps") { +"Run Steps" } }
                li(classes = if (section == "post-run-steps") "active" else "") { a(href = "/edit-job/${job.id}/post-run-steps") { +"Post-Run Steps" } }
                li(classes = if (section == "scheduling") "active" else "") { a(href = "/edit-job/${job.id}/scheduling") { +"Scheduling" } }
                li(classes = if (section == "parameters") "active" else "") { a(href = "/edit-job/${job.id}/parameters") { +"Parameters" } }
                li(classes = if (section == "revisions") "active" else "") { a(href = "/edit-job/${job.id}/revisions") { +"Revisions" } }
            }
        }

        if (section == "run-steps") {
            div(classes = "tab-container") {
                id = "run-steps-container"

                div {
                    id = "run-steps"

                    div(classes = "run-step-drop-zone root-drop-zone") {
                        /*div(classes = "no-run-steps-message") {
                            +"Drag a "
                            strong { +"run step" }
                            +" from the right and drop it here to configure the actions this job will perform."
                        }*/

                        div(classes = "drop-zone-row") {
                            div(classes = "run-step-drop-zone") {
                                div(classes = "run-step") {
                                    h3 {
                                        div(classes = "icon icon-16 icon-mobile")
                                        div(classes = "name") { +"Start Device Emulator" }
                                    }
                                    div(classes = "run-step-body") {
                                        div(classes = "start-emulator-run-step") {

                                        }
                                    }
                                }
                            }
                            div(classes = "run-step-drop-zone drop-zone-column") {
                                div(classes = "run-step") {
                                    h3 {
                                        div(classes = "icon icon-16 icon-code")
                                        div(classes = "name") { +"Clone Repository" }
                                    }
                                    div(classes = "run-step-body") {
                                        div(classes = "clone-repo-run-step") {
                                            div(classes = "no-items") { +"You haven't added any repositories" }
                                            a(href = "#", classes = "button") {
                                                div(classes = "icon icon-10 icon-plus")
                                                div { +"Add Repository" }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                div {
                    id = "available-run-steps"

                    div(classes = "run-step run-step-inactive") {
                        div(classes = "icon icon-12 icon-stopwatch1 profiler-icon") {
                            title = "Execution time of this step will be profiled"
                        }
                        h3 {
                            div(classes = "icon icon-16 icon-code")
                            div(classes = "name") { +"Clone Repository" }
                        }
                    }

                    div(classes = "run-step run-step-inactive") {
                        h3 {
                            div(classes = "icon icon-16 icon-spinner9")
                            div(classes = "name") { +"Send Build Status" }
                        }
                    }

                    for (plugin in Kokoro.pluginManager.plugins) {
                        for (runStep in plugin.runSteps) {
                            try {
                                val runStepName = runStep.name
                                val runStepIcon = runStep.icon

                                div(classes = "run-step run-step-inactive run-step-template") {
                                    attributes["data-plugin"] = plugin.hash
                                    h3 {
                                        div(classes = "icon icon-16 icon-$runStepIcon")
                                        div(classes = "name") { +runStepName }
                                    }
                                    div(classes = "run-step-body")
                                }
                            } catch (error: Throwable) {
                                error.printStackTrace()
                            }
                        }
                    }
                }
            }
        } else if (section == "scheduling") {
            div(classes = "tab-container") {
                id = "scheduling-container"

                div {
                    id = "run-steps"

                    div(classes = "run-step-drop-zone") {
                        +"Drag a "
                        strong { +"run step" }
                        +" from the right and drop it here to configure the actions this job will perform."
                    }
                }
            }
        }

        /*val process = Runtime.getRuntime().exec(arrayOf("bash", "-c", "compgen -c"))

        process.waitFor()
        val commandCompletions = process.inputStream.reader()
                .readText()
                .split("\n")
                .mapNotNull {
                    if (it.isBlank()) null else it.trim()
                }

        val envProcess = Runtime.getRuntime().exec(arrayOf("env"))

        envProcess.waitFor()
        val envCompletions = envProcess.inputStream.reader()
                .readText()
                .split("\n")
                .mapNotNull {
                    if ('=' in it) "$" + it.split("=").first() else null
                }

        val completions = (commandCompletions + envCompletions).joinToString(",") { "\"$it\"" }

        val rawJs = "var bashCommands = [$completions];"

        script { unsafe { raw(rawJs) } }*/

        script(src = "/static/js/ace/ace.js") {}
        script(src = "/static/js/ace/mode-sh.js") {}
        script(src = "/static/js/ace/theme-xcode.js") {}

        script(src = "/static/js/job-configure-page.js") {}
        script(src = "/static/js/run-steps.js") {}
    }
}
