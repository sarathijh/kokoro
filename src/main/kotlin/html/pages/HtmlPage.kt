package html.pages

import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import kotlinx.html.*

abstract class HtmlPage {
    enum class Sidebar {
        Dashboard,
        Jobs,
        Repositories,
        Credentials,
        Plugins,
        Settings,
    }

    abstract val pageTitle: String
    abstract val iconClass: String
    abstract val activeSidebarItem: Sidebar

    suspend fun respondHtml(call: ApplicationCall) {
        call.respondHtml {
            head {
                title { +"$pageTitle | Kokoro" }

                meta(name = "viewport", content = "width=device-width, initial-scale=1")

                link(rel = "icon", href = "/static/images/favicon.ico", type = "image/x-icon")

                styleLink("/static/css/reset.css")
                styleLink("/static/css/common.css")
                styleLink("/static/css/icons.css")
                styleLink("/static/css/global-search.css")

                head()
            }

            body {
                div(classes = "wrapper") {
                    sidebar()

                    div(classes = "content") {
                        div(classes = "page-header") {
                            h2 {
                                //div(classes = "icon icon-24 $iconClass")
                                div(classes = "name") { +pageTitle }
                            }

                            header()

                            div(classes = "flex-space")

                            headerRight()
                            //a(href = "#", classes = "icon icon-24 icon-help") { id = "help-button" }
                        }

                        /*div {
                            id = "help-tray"

                            div(classes = "help-container") {
                                helpContent()
                            }
                        }*/

                        div(classes = "content-body") {
                            content()
                        }
                    }
                }

                foot()

                script(src = "/static/js/global-search.js") {}
                script(src = "/static/js/help-tray.js") {}
            }
        }
    }

    protected open fun HEAD.head() {}
    protected open fun BODY.foot() {}
    protected open fun DIV.header() {}
    protected open fun DIV.headerRight() {}
    protected open fun DIV.content() {}
    protected open fun DIV.helpContent() {}

    private fun DIV.sidebar() {
        div(classes = "sidebar") {
            div(classes = "sidebar-content") {
                h1(classes = "kokoro-brand") {
                    img(src = "/static/images/heart-logo.svg")
                    div(classes = "heading") {
                        span(classes = "title") { +"Kokoro" }
                        //span(classes = "version") { +"1.0.0-dev" }
                    }
                }

                ul {
                    li {
                        a(href = "/dashboard") {
                            if (activeSidebarItem == Sidebar.Dashboard) {
                                classes += "active"
                            }
                            div(classes = "icon icon-dashboard")
                            div(classes = "title") { +"Dashboard" }
                        }
                    }
                }
                ul {
                    li {
                        a(href = "/jobs") {
                            if (activeSidebarItem == Sidebar.Jobs) {
                                classes += "active"
                            }
                            div(classes = "icon icon-build")
                            div(classes = "title") { +"Jobs" }
                        }
                    }
                    li {
                        a(href = "/repositories") {
                            if (activeSidebarItem == Sidebar.Repositories) {
                                classes += "active"
                            }
                            div(classes = "icon icon-code")
                            div(classes = "title") { +"Repositories" }
                        }
                    }
                    li {
                        a(href = "/credentials") {
                            if (activeSidebarItem == Sidebar.Credentials) {
                                classes += "active"
                            }
                            div(classes = "icon icon-key")
                            div(classes = "title") { +"Credentials" }
                        }
                    }
                }
                ul {
                    li {
                        a(href = "/plugins") {
                            if (activeSidebarItem == Sidebar.Plugins) {
                                classes += "active"
                            }
                            div(classes = "icon icon-plugin")
                            div(classes = "title") { +"Plugins" }
                        }
                    }
                    li {
                        a(href = "/settings") {
                            if (activeSidebarItem == Sidebar.Settings) {
                                classes += "active"
                            }
                            div(classes = "icon icon-gear")
                            div(classes = "title") { +"Settings" }
                        }
                    }
                }
            }
        }
    }
}
