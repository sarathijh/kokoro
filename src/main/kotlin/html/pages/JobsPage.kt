package html.pages

import database.Job
import database.RunStatus
import database.RunStatus.*
import database.lastRun
import kotlinx.html.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.format.DateTimeFormat
import java.text.SimpleDateFormat
import java.util.*

class JobsPage : ItemsTablePage<Job>() {
    override val namePlural = "Jobs"
    override val nameSingular = "Job"
    override val description = "Jobs..."
    override val iconClass = "icon-build"
    override val activeSidebarItem = Sidebar.Jobs

    override fun HEAD.styleSheets() {
        styleLink("/static/css/jobs-page.css")
    }

    override fun BODY.foot() {
        script(src = "/static/js/Chart.bundle.min.js") {}
        script(src = "/static/js/jobs-page.js") {}
    }

    override val items = transaction { Job.all().sortedBy { it.name } }

    override fun itemCssClasses(item: Job) = when (item.lastRun?.status) {
        Passed -> "status-success"
        Failed -> "status-failed"
        Aborted -> "status-aborted"
        InProgress -> "status-running"
        null -> "status-none"
    }

    val RunStatus?.iconCssClass get() = when (this) {
        Passed -> "icon-checkmark"
        Failed -> "icon-cross"
        Aborted -> "icon-warning"
        InProgress -> "icon-spinner11"
        null -> ""
    }

    val RunStatus?.title get() = when (this) {
        Passed -> "Last run succeeded"
        Failed -> "Last run failed"
        Aborted -> "Last run was aborted"
        InProgress -> "Running..."
        null -> "This job has not been run yet"
    }

    override val columns = listOf(

            Column("", "status") { job ->
                val status = job.lastRun?.status

                div(classes = "status-icon ${status.iconCssClass}") {
                    title = status.title
                }
            },

            Column("Job", "job") { job ->
                a(href = "/job/${job.id}", classes = "job-name") { +job.name }

                ul(classes = "item-actions") {
                    li { a(href = "/edit-job/${job.id}/run-steps", classes = "action-edit") { +"Edit" } }
                    if (job.lastRun?.status == InProgress) {
                        li { a(href = "#", classes = "action-abort") { +"Abort" } }
                    } else {
                        li { a(href = "#", classes = "action-disable") { +"Disable" } }
                        li {
                            a(href = "#", classes = "action-run") {
                                attributes["data-job-id"] = "${job.id}"

                                +"Run"
                            }
                        }
                    }
                }
            },

            Column("Metrics", "metrics") { job ->
                val runs = transaction { job.runs.toList() }

                val successfulRuns = runs.filter { it.status == Passed }
                val failedRuns = runs.filter { it.status == Failed }
                val abortedRuns = runs.filter { it.status == Aborted }

                div(classes = "status-container") {
                    div(classes = "status-chart-container") {
                        canvas(classes = "status-chart") {
                            width = "32"
                            height = "32"

                            attributes["data-success"] = successfulRuns.size.toString()
                            attributes["data-failure"] = failedRuns.size.toString()
                            attributes["data-aborted"] = abortedRuns.size.toString()
                        }
                    }
                    div(classes = "status-numbers") {
                        div { +"${runs.size} runs" }
                        if (runs.isNotEmpty()) {
                            div { +"${successfulRuns.size * 100 / runs.size}% success" }
                        }
                    }
                }
            },

            Column("Description", "description") { job ->
                +"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam consequat eros vitae placerat pulvinar."
            },

            Column("Last Run", "last-run") { job ->
                val date = transaction { job.lastRun?.startDatetime }
                if (date != null) {
                    +DateTimeFormat.forPattern("MM/dd/yyyy").print(date)
                } else {
                    +"-"
                }
            },

            Column("Created", "created") { job ->
                +SimpleDateFormat("MM/dd/yyyy").format(Date())
            }
    )
}
