package html.pages

import kotlinx.html.DIV

class Credential

class CredentialsPage : ItemsTablePage<Credential>() {
    override val namePlural = "Credentials"
    override val nameSingular = "Credential"
    override val description = "Credentials let your jobs authenticate to third-party services, such as GitHub."
    override val iconClass = "icon-key"
    override val activeSidebarItem = Sidebar.Credentials

    override val items = listOf<Credential>()

    override val columns = listOf<Column>()

    override fun DIV.helpContent() {
        +"Saved credentials allow your jobs to automatically authenticate to third-party services."
    }
}
