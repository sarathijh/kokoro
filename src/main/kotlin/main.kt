import com.google.gson.Gson
import database.Job
import database.badgeFile
import html.pages.*
import io.ktor.application.call
import io.ktor.content.PartData
import io.ktor.content.files
import io.ktor.content.static
import io.ktor.content.staticRootFolder
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.request.receiveMultipart
import io.ktor.response.respond
import io.ktor.response.respondBytes
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.html.div
import kotlinx.html.stream.appendHTML
import org.jetbrains.exposed.sql.transactions.transaction
import plugin.SandboxSecurityPolicy
import java.io.File

fun main(args: Array<String>) {

    SandboxSecurityPolicy.start()

    val server = embeddedServer(Netty, Kokoro.port) {
        routing {
            get("/") {
                DashboardPage().respondHtml(call)
            }

            get("/dashboard") {
                DashboardPage().respondHtml(call)
            }

            get("/jobs") {
                JobsPage().respondHtml(call)
            }

            get("/job/{jobId}") {
                val jobId = call.parameters["jobId"]?.toInt() ?: return@get
                val job = transaction { Job.findById(jobId) } ?: return@get
                JobOverviewPage(job).respondHtml(call)
            }

            get("/job/{jobId}/badge") {
                val jobId = call.parameters["jobId"]?.toInt() ?: return@get
                val job = transaction { Job.findById(jobId) } ?: return@get
                call.respondBytes(job.badgeFile.readBytes(), ContentType.parse("image/svg+xml"))
            }

            get("/edit-job/{jobId}/{section}") {
                val jobId = call.parameters["jobId"]?.toInt() ?: return@get
                val job = transaction { Job.findById(jobId) } ?: return@get
                val section = call.parameters["section"] ?: return@get
                if (section !in listOf("run-steps", "post-run-steps", "scheduling", "parameters", "revisions")) {
                    return@get
                }
                EditJobPage(job, section).respondHtml(call)
            }

            get("/credentials") {
                CredentialsPage().respondHtml(call)
            }

            get("/search/{query}") {
                val query = call.parameters["query"] ?: return@get
                call.respond(Gson().toJson(GlobalSearch.queryAction(query)))
            }

            get("/repositories") {
                RepositoriesPage().respondHtml(call)
            }

            get("/plugins") {
                PluginsPage().respondHtml(call)
            }

            get("/settings") {
                SettingsPage().respondHtml(call)
            }

            get("/emulators") {
                when (call.parameters["os"]) {
                    "Android" -> {
                        val process = Runtime.getRuntime().exec(arrayOf("${System.getProperty("user.home")}/Library/Android/sdk/emulator/emulator", "-list-avds"))
                        process.waitFor()
                        val result = process.inputStream.reader()
                                .readText()
                                .split("\n")
                                .mapNotNull {
                                    if (it.isBlank()) null else it.trim()
                                }
                                .joinToString("<br />")
                        call.respondText(result)
                    }
                    "iOS" -> {
                        val process = Runtime.getRuntime().exec(arrayOf("instruments", "-s", "devices"))
                        process.waitFor()
                        val result = process.inputStream.reader()
                                .readText()
                                .split("\n")
                                .mapNotNull {
                                    if (it.isBlank()) null else it.trim()
                                }
                                .filter { it.matches("^(iPhone|iPad).*$".toRegex()) }
                                .filter { '+' !in it }
                                .map { it.substring(0, it.indexOf('[')) }
                                .joinToString("<br />")
                        call.respondText(result)
                    }
                    else -> return@get
                }
            }

            post("/add-plugin") {
                val input = call.receiveMultipart()
                val fileName = input.readPart() as? PartData.FormItem ?: return@post
                val fileItem = input.readPart() as? PartData.FileItem ?: return@post

                Kokoro.pluginManager.addPlugin(fileName.value, fileItem.streamProvider())

                call.respond(status = HttpStatusCode.Accepted, message = "")
            }

            get("/delete-plugin") {
                val pluginHash = call.parameters["plugin"] ?: return@get

                Kokoro.pluginManager.deletePlugin(pluginHash)

                call.respond(status = HttpStatusCode.Accepted, message = "")
            }

            get("/api/plugin/{hash}/editorHtml") {
                val hash = call.parameters["hash"] ?: return@get
                val plugin = Kokoro.pluginManager.hashToPlugin[hash] ?: return@get

                call.respondText(buildString {
                    appendHTML().div {
                        plugin.runSteps.first().editorHtml(this)
                    }
                })
            }

            post("/api/run-job/{jobId}") {
                val jobId = call.parameters["jobId"]?.toInt() ?: return@post
                val job = transaction { Job.findById(jobId) } ?: return@post
                JobRunner.run(job)

                call.respond(HttpStatusCode.Accepted, "")
            }

            static("static") {
                staticRootFolder = File(Kokoro.homeDirectory, "static")

                folder("images")
                folder("css")
                folder("js")
                folder("fonts")
            }
        }
    }

    server.start(wait = true)
}

fun Route.folder(remotePath: String) {
    static(remotePath) { files(remotePath) }
}
