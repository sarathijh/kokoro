import database.Configurations
import database.Jobs
import database.Repositories
import database.Runs
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction
import plugin.PluginManager
import java.io.File
import java.sql.Connection

object Kokoro {
    val homeDirectory = File(System.getenv("KOKORO_HOME") ?: throw Exception("KOKORO_HOME not defined!"))
    val port = System.getenv("KOKORO_PORT")?.toIntOrNull() ?: 8080

    private val databaseFile = File(homeDirectory, "kokoro.sqlite")

    val pluginManager by lazy {
        val pluginsDirectory = File(homeDirectory, "plugins")
        pluginsDirectory.mkdirs()
        PluginManager(pluginsDirectory.toPath())
    }

    init {
        pluginManager.plugins
        connectToDatabase()
        registerGlobalSearchActions()
    }

    private fun registerGlobalSearchActions() {
        GlobalSearch.register("Running Jobs", GlobalSearch.Action.NavigateToUrl("/jobs?filter=running"))
    }

    private fun connectToDatabase() {
        Database.connect("jdbc:sqlite:${databaseFile.absolutePath}", driver = "org.sqlite.JDBC")
        TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE

        transaction {
            SchemaUtils.createMissingTablesAndColumns(Jobs, Configurations, Runs, Repositories)
        }
    }
}
