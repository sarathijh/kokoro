package plugin

import kotlinx.coroutines.experimental.cancelAndJoin
import kotlinx.coroutines.experimental.launch
import java.io.File
import java.nio.file.Path
import java.nio.file.StandardWatchEventKinds.*
import java.nio.file.WatchService
import java.util.regex.Pattern

abstract class PathWatcher(private val path: Path, patternString: String = ".+") {

    protected abstract fun onFileCreated(file: File)
    protected abstract fun onFileModified(file: File)
    protected abstract fun onFileDeleted(file: File)

    private val regex = Pattern.compile(patternString).toRegex()

    private val watcher by lazy {
        path.watch()
    }

    private val job = launch {
        while (true) {
            val key = watcher.take()

            key.pollEvents().forEach { it ->
                val file = File(path.toFile(), it.context().toString())
                if (regex.matches(file.name)) {
                    when (it.kind()) {
                        ENTRY_CREATE -> onFileCreated(file)
                        ENTRY_MODIFY -> onFileModified(file)
                        ENTRY_DELETE -> onFileDeleted(file)
                    }
                }
            }

            key.reset()
        }
    }

    private fun Path.watch(): WatchService {
        val watchService = fileSystem.newWatchService()
        register(watchService, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE)
        return watchService
    }

    suspend fun stop() {
        job.cancelAndJoin()
    }
}
