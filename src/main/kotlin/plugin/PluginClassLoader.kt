package plugin

import java.net.URL
import java.net.URLClassLoader

class PluginClassLoader(jarFileUrl: URL) : URLClassLoader(arrayOf(jarFileUrl))
