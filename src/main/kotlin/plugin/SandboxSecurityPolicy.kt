package plugin

import java.security.*

class SandboxSecurityPolicy : Policy() {

    companion object {
        fun start() {
            Policy.setPolicy(SandboxSecurityPolicy())
            System.setSecurityManager(SecurityManager())
        }
    }

    override fun getPermissions(domain: ProtectionDomain): PermissionCollection {
        return if (isPlugin(domain)) {
            pluginPermissions()
        } else {
            applicationPermissions()
        }
    }

    private fun isPlugin(domain: ProtectionDomain) = domain.classLoader is PluginClassLoader

    private fun pluginPermissions() = Permissions()

    private fun applicationPermissions() = Permissions().apply {
        add(AllPermission())
    }
}
