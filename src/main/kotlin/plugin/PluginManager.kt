package plugin

import KokoroPlugin
import kotlinx.coroutines.experimental.launch
import java.io.File
import java.io.InputStream
import java.nio.file.Path

class PluginManager(private val pluginsDirectory: Path) : PathWatcher(pluginsDirectory, ".+\\.jar") {

    val plugins get() = jarToPlugin.values
    val jarToPlugin = mutableMapOf<File, PluginWrapper>()
    val hashToPlugin = mutableMapOf<String, PluginWrapper>()

    init {
        launch {
            pluginsDirectory.toFile().walk()
                    .maxDepth(1)
                    .filter { it.extension == "jar" }
                    .forEach(::loadPlugin)
        }
    }

    fun addPlugin(fileName: String, inputStream: InputStream) {
        println("Plugin added: $fileName")
        inputStream.use { input ->
            File(pluginsDirectory.toFile(), fileName).outputStream().use { output ->
                input.copyTo(output)
            }
        }
    }

    fun deletePlugin(hash: String) {
        hashToPlugin[hash]?.jarFile?.delete()
    }

    override fun onFileCreated(file: File) {
        println("${file.name} created in plugins folder")
        loadPlugin(file)
    }

    override fun onFileModified(file: File) {
        println("${file.name} modified in plugins folder")
        unloadPlugin(file)
        loadPlugin(file)
    }

    override fun onFileDeleted(file: File) {
        println("${file.name} deleted from plugins folder")
        unloadPlugin(file)
    }

    private fun loadPlugin(jarFile: File) {
        val pluginLoader = PluginClassLoader(jarFile.toURI().toURL())
        val pluginClass = pluginLoader.loadClass("Plugin")
        val plugin = pluginClass.newInstance() as? KokoroPlugin

        if (plugin != null) {
            val wrapper = PluginWrapper(jarFile, plugin)

            val existingPluginForHash = hashToPlugin[wrapper.hash]
            if (existingPluginForHash != null) {
                println("Plugin not loaded: ${plugin.name} hash (${wrapper.hash}) conflicts with existing plugin: ${existingPluginForHash.name}")
                return
            }

            jarToPlugin[jarFile] = wrapper
            hashToPlugin[wrapper.hash] = wrapper

            println("Plugin loaded: ${plugin.name} (${wrapper.hash}) from file: ${jarFile.name}")
        } else {
            println("Plugin not loaded: ${jarFile.name} does not implement the KokoroPlugin interface")
        }
    }

    private fun unloadPlugin(jarFile: File) {
        val plugin = jarToPlugin[jarFile]

        if (plugin != null) {
            println("Plugin unloaded: ${plugin.name} (${plugin.hash}) from file: ${jarFile.name}")
            jarToPlugin.remove(jarFile)
            hashToPlugin.remove(plugin.hash)
        } else {
            println("Plugin not unloaded: ${jarFile.name} because it wasn't loaded to begin with")
        }
    }
}
