package plugin

import KokoroPlugin
import java.io.File
import java.security.MessageDigest

class PluginWrapper(val jarFile: File, plugin: KokoroPlugin) : KokoroPlugin by plugin {

    companion object {
        const val HEX_CHARS = "0123456789ABCDEF"
    }

    val hash: String by lazy {
        val digestBytes = MessageDigest.getInstance("MD5").digest(jarFile.readBytes())

        val result = StringBuilder(digestBytes.size * 2)

        digestBytes.forEach {
            val index = it.toInt()
            result.append(HEX_CHARS[index shr 4 and 0x0f])
            result.append(HEX_CHARS[index and 0x0f])
        }

        result.toString()
    }
}
