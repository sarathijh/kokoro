import kotlinx.html.DIV

interface RunStep {
    val name: String
    // TODO: Make this an enum of valid icons
    val icon: String

    fun editorHtml(divContainer: DIV)
    fun run()
}
