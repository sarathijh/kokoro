interface KokoroPlugin {
    val name: String
    val author: String
    val version: Version
    val description: String

    val runSteps get() = listOf<RunStep>()
}
