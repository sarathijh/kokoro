class Plugin : KokoroPlugin {
    override val name = "Shell Script"
    override val author = "Kokoro"
    override val version = Version(1, 0, 0)
    override val description = "Adds a run step that allows jobs to execute shell scripts."

    override val runSteps = listOf(ShellScriptRunStep)
}
