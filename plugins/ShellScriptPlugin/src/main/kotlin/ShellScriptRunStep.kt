import kotlinx.html.DIV
import kotlinx.html.div

object ShellScriptRunStep : RunStep {
    override val name = "Shell Script"
    override val icon = "terminal"

    override fun editorHtml(divContainer: DIV) {
        with(divContainer) {
            div(classes = "script-editor")
        }
    }

    override fun run() {

    }
}
