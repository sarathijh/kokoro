import kotlinx.html.DIV

object StartEmulatorRunStep : RunStep {
    override val name = "Start Emulator"
    override val icon = "mobile"

    override fun editorHtml(divContainer: DIV) {
        with(divContainer) {
            +"Start emulator"
        }
    }

    override fun run() {

    }
}
