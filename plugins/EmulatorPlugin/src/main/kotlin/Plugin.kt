class Plugin : KokoroPlugin {
    override val name = "Device Emulator"
    override val author = "Kokoro"
    override val version = Version(1, 0, 0)
    override val description = "Adds a run step that lets jobs create and boot up emulators for Android and iOS."

    override val runSteps = listOf(StartEmulatorRunStep)
}
